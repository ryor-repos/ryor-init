'use strict'

function description()
{
  const {bold} = require('chalk')

  return `Verifies that production build completes successfully (testing and Codecov upload coming soon)`
}

const run = [
  'log -w Verifying that production build completes successfully',
  'build -s',
  'log -s Build completed successfully'
]

module.exports = {description, run}
