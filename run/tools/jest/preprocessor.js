const {transpile} = require('typescript')

module.exports = {
  process: function(source, path)
  {
    if (path.endsWith('.ts'))
    {
      let js = transpile(source, {module:'CommonJS', target:'ES2017'}, path)

      if (js.includes('download_1.default'))
        js = js.replace(/download_1\.default/g, 'download_1')

      if (js.includes('prompt_base_1.default'))
        js = js.replace(/prompt_base_1\.default/g, 'prompt_base_1')

      if (js.includes('prompt_list_1.default'))
        js = js.replace(/prompt_list_1\.default/g, 'prompt_list_1')

      if (js.includes('strip_ansi_1.default'))
        js = js.replace(/strip_ansi_1\.default/g, 'strip_ansi_1')

      return js
    }

    return source
  }
}
