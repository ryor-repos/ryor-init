import {EOL} from 'os'

export default {
  input: 'build/esm/index.js',
  output: {
    esModule: false,
    file: 'build/index.js',
    format: 'cjs'
  },
  external: ['chalk', 'download', 'fs-extra', 'os', 'path', 'prompt-base', 'prompt-list', 'shelljs'],
  plugins: [{
    renderChunk: source => source
      .replace(`'use strict';${EOL}${EOL}Object.defineProperty(exports, '__esModule', { value: true });`, `#!/usr/bin/env node${EOL}${EOL}'use strict';`)
      .replace('exports.run = run;', 'run();')
  }]
}
