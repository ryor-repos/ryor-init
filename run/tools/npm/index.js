'use strict'

const REQUIRED_ENVIRONMENT_VARIABLE = 'NPM_TOKEN'

const description = 'Runs preconfigured NPM publish command'

function usage()
{
  return require('../../utils/usage').composeUsageInformation(undefined, [
    ['publish', 'Publishes latest build to NPM']
  ])
}

function run(args)
{
  const {bold} = require('chalk')

  if (args.length === 0 || args[0] !== 'publish')
  {
    let errorMessage = `The ${bold('publish')} command is required to use the ${bold('npm')} tool.`

    if (args.length > 0)
      errorMessage += ` Received ${bold(args[0])}.`

    throw new Error(errorMessage)
  }

  const {existsSync} = require('fs')
  const {resolve} = require('path')
  const buildDirectoryPath = resolve(process.cwd(), 'build')

  if (!existsSync(buildDirectoryPath)
  || !existsSync(resolve(buildDirectoryPath, 'index.js')))
    throw new Error(`Module not found. Make sure to run ${bold('build')} before running ${bold('npm publish')}.`)

  if (!process.env.NPM_TOKEN)
    throw new Error(`${bold('NPM_TOKEN')} environment variable required for ${bold('npm publish')} is not set.`)

  const {readFileSync, writeFileSync} = require('fs')
  const {EOL} = require('os')
  const packageJSON = require('../../../package.json')

  delete packageJSON.devDependencies
  packageJSON.bin = {ryor:'index.js'}

  writeFileSync(resolve(buildDirectoryPath, 'package.json'), JSON.stringify(packageJSON, null, 2))

  return [
    'log -w Publishing to NPM',
    'shx cp -rf README.md run/tools/npm/.npmrc build',
    'cd build',
    'npm publish',
    'cd ..'
  ]
}

module.exports = {description, run, usage}
