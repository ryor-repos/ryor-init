## Command-line utility for initializing projects from ryor templates

![pipeline](https://gitlab.com/ryor-/ryor-init/badges/master/pipeline.svg)
![coverage](https://gitlab.com/ryor-/ryor-init/badges/master/coverage.svg)
![license](https://img.shields.io/badge/License-MIT-green.svg)

### Requirements:

Node >= 7.6.0

### Install:

  `npm install -g `

### Use:

  `npx ryor-init`

or

  `npm install -g ryor-init`
  `ryor-init`
