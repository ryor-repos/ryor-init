import {existsSync, readdirSync, statSync} from 'fs-extra'
import {resolve} from 'path'
import Prompt from 'prompt-base'
import List from 'prompt-list'

const Choice:{[key:string]:string} =
{
  Confirm: 'Yes',
  Decline: 'Choose another location...'
}

const Message:{[key:string]:string} =
{
  ConfirmBackupPath: 'A [PATH_TYPE] exists at [PROJECT_PATH] so it will be moved to [BACKUP_PATH]. Cool?',
  ConfirmProjectPath: 'Use [PROJECT_PATH]?',
  EnterProjectPath: 'Location (relative to current directory [CURRENT_DIRECTORY]):',
  Initial: 'Where would you like to create your project?'
}

export async function selectPaths():Promise<Paths>
{
  const paths:Paths = {project:process.cwd()}
  const choice:string = await new List({
    message: Message.Initial,
    name: 'choice',
    choices: [paths.project, Choice.Decline]
  }).run()

  if (choice !== paths.project)
    return selectNewPaths()

  return addBackupPathIfNecessary(paths)
}

export async function selectNewPaths():Promise<Paths>
{
  const cwd:string = process.cwd()
  const input:string|undefined = await new Prompt({
    message: Message.EnterProjectPath.replace('[CURRENT_DIRECTORY]', cwd),
    name: 'input'
  }).run()
  const paths:Paths = {project: input === undefined ? cwd : resolve(cwd, input)}
  const confirmed:string = await new List({
    message: Message.ConfirmProjectPath.replace('[PROJECT_PATH]', paths.project),
    name: 'confirmed',
    choices: [Choice.Confirm, Choice.Decline]
  }).run()

  if (confirmed !== Choice.Confirm)
    return selectNewPaths()

  return addBackupPathIfNecessary(paths)
}

export async function addBackupPathIfNecessary(paths:Paths):Promise<Paths>
{
  if (!existsSync(paths.project)
  || (statSync(paths.project).isDirectory() && readdirSync(paths.project).length === 0))
    return paths

  let index:number = 0

  paths.backup = `${paths.project}.bak`

  while (existsSync(paths.backup))
    paths.backup = `${paths.project}.bak${++index}`

  const confirmed:string = await new List({
    message: Message.ConfirmBackupPath
      .replace('[PATH_TYPE]', statSync(paths.project).isFile() ? 'file' : 'directory')
      .replace('[PROJECT_PATH]', paths.project)
      .replace('[BACKUP_PATH]', paths.backup),
    name: 'confirmed',
    choices: [Choice.Confirm, Choice.Decline]
  }).run()

  if (confirmed !== Choice.Confirm)
    return selectNewPaths()

  return paths
}
