const {bold, cyan} = require('chalk')
const {done, okay, wait} = require('../log')

test('Outputs wait message', () =>
{
  const stdoutWriteMock = jest.spyOn(process.stdout, 'write').mockImplementation((value) => stdoutWriteValue = value)
  let stdoutWriteValue = ''

  wait('Message')

  expect(stdoutWriteValue).toBe(`${cyan('⏳')} ${bold('Message')}`)

  stdoutWriteMock.mockRestore()
})

test('Outputs done messages', () =>
{
  const consoleLogMock = jest.spyOn(console, 'log').mockImplementation((value) => consoleLogValue = value)
  let consoleLogValue = ''

  done()

  expect(consoleLogValue).toBe(` ${cyan('done')}`)

  done('Message')

  expect(consoleLogValue).toBe(` ${cyan('Message')}`)

  consoleLogMock.mockRestore()
})

test('Outputs okay message', () =>
{
  const consoleLogMock = jest.spyOn(console, 'log').mockImplementation((value) => consoleLogValue = value)
  let consoleLogValue = ''

  okay('Message')

  expect(consoleLogValue).toBe(`${cyan('☺')} ${bold('Message')}`)

  consoleLogMock.mockRestore()
})
