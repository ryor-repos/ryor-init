const {EOL} = require('os')
const {resolve} = require('path')
const {addBackupPathIfNecessary, selectNewPaths, selectPaths} = require('../paths')
const DOWN = '\u001b[B'

test('Chooses to initialize project in current working directory', async () =>
{
  let projectPathConfirmed = false
  let backupPathConfirmed = false

  const stdoutWriteMock = jest.spyOn(process.stdout, 'write').mockImplementation(value =>
  {
    if (value.includes('Where') && !projectPathConfirmed)
    {
      projectPathConfirmed = true

      process.stdin.emit('keypress', EOL)
    }

    else if (value.includes('Cool') && !backupPathConfirmed)
    {
      backupPathConfirmed = true

      process.stdin.emit('keypress', EOL)
    }
  })

  const paths = await selectPaths()

  expect(paths).toEqual({
    project: process.cwd(),
    backup: `${process.cwd()}.bak`
  })

  stdoutWriteMock.mockRestore()
})

test('Declines to initialize project in current working directory and chooses another project path', async () =>
{
  const input = 'project/path'
  let inputEntered = false
  let projectPathDenied = false
  let projectPathConfirmed = false

  const stdoutWriteMock = jest.spyOn(process.stdout, 'write').mockImplementation(value =>
  {
    if (value.includes('Where') && !projectPathDenied)
    {
      projectPathDenied = true

      process.stdin.emit('keypress', `${DOWN}${EOL}`)
    }

    else if (value.includes('Location') && !inputEntered)
    {
      inputEntered = true

      process.stdin.emit('keypress', `${input}${EOL}`)
    }

    else if (value.includes('Use') && !projectPathConfirmed)
    {
      projectPathConfirmed = true

      process.stdin.emit('keypress', EOL)
    }
  })

  const paths = await selectPaths()

  expect(paths).toEqual({project: resolve(process.cwd(), input)})

  stdoutWriteMock.mockRestore()
})

test('Selects new paths', async () =>
{
  const input = 'project/path'
  let inputEntered = false
  let projectPathConfirmed = false

  const stdoutWriteMock = jest.spyOn(process.stdout, 'write').mockImplementation(value =>
  {
    if (value.includes('Location') && !inputEntered)
    {
      inputEntered = true

      process.stdin.emit('keypress', `${input}${EOL}`)
    }

    else if (value.includes('Use') && !projectPathConfirmed)
    {
      projectPathConfirmed = true

      process.stdin.emit('keypress', EOL)
    }
  })

  const paths = await selectNewPaths()

  expect(paths).toEqual({project: resolve(process.cwd(), input)})

  stdoutWriteMock.mockRestore()
})

test('Adds backup path if necessary', async () =>
{
  let backupPathConfirmed = false

  const stdoutWriteMock = jest.spyOn(process.stdout, 'write').mockImplementation(value =>
  {
    if (value.includes('Cool') && !backupPathConfirmed)
    {
      backupPathConfirmed = true

      process.stdin.emit('keypress', EOL)
    }
  })

  const paths = await addBackupPathIfNecessary({project:process.cwd()})

  expect(paths).toEqual({
    project: process.cwd(),
    backup: `${process.cwd()}.bak`
  })

  stdoutWriteMock.mockRestore()
})
