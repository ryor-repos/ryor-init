const {EOL} = require('os')
const {resolve} = require('path')
const DOWN = '\u001b[B'

jest.setMock('download', (url) => new Promise(resolve =>
{
  if (url === URL.TemplatesList)
    resolve('[{"type": "templates", "templates": ["ryor-org/ryor-template-1", "ryor-org/ryor-template-2"]}]')

  else if (url.includes('ryor-org/ryor-template-1'))
    resolve('[]')

  else if (url.includes('ryor-org/ryor-template-2'))
    resolve('[{"name": "1.0.0"}, {"name": "1.0.1"}, {"name": "1.0.2"}]')
}))

const {Message, URL, fetchRepositoryTags, fetchTemplatesList, selectLatestTemplateVersion, selectOtherTemplateVersion, selectTemplateVersion} = require('../template')

test('Fetches templates list', async () =>
{
  const templatesList = await fetchTemplatesList()

  expect(templatesList).toEqual([{
    type: 'templates',
    templates: [
      'ryor-org/ryor-template-1',
      'ryor-org/ryor-template-2'
    ]
  }])
})

test('Fetches repository tags', async () =>
{
  let repositoryTags

  repositoryTags = await fetchRepositoryTags('ryor-org/ryor-template-1')

  expect(repositoryTags).toEqual([])

  repositoryTags = await fetchRepositoryTags('ryor-org/ryor-template-2')

  expect(repositoryTags).toEqual(['1.0.0', '1.0.1', '1.0.2'])
})

test('Selects template versions', async () =>
{
  const stdoutWriteMock = jest.spyOn(process.stdout, 'write').mockImplementation((value) =>
  {
    if (value.includes(Message.ChooseVersionLatest.replace('[VERSION]', '1.0.0')) && !choiceMade)
    {
      choiceMade = true
      choicesMade++

      process.stdin.emit('keypress', choicesMade === 1 ? EOL : `${DOWN}${EOL}${EOL}`)
    }
  })
  let choiceMade = false
  let choicesMade = 0
  let version = await selectTemplateVersion('ryor-org/ryor-template-1')

  expect(version).toBe('master')

  choiceMade = false
  version = await selectTemplateVersion('ryor-org/ryor-template-2')

  expect(version).toBe('1.0.0')

  stdoutWriteMock.mockRestore()
})

test('Selects or declines to use latest template version', async () =>
{
  const stdoutWriteMock = jest.spyOn(process.stdout, 'write').mockImplementation((value) =>
  {
    if (value.includes(Message.ChooseVersionLatest.replace('[VERSION]', version)) && !choiceMade)
    {
      choiceMade = true
      choicesMade++

      process.stdin.emit('keypress', choicesMade === 1 ? EOL : `${DOWN}${EOL}`)
    }
  })
  const version = '0.0.1'
  let choiceMade = false
  let choicesMade = 0
  let useLatestVersion = await selectLatestTemplateVersion(version)

  expect(useLatestVersion).toBe(true)

  choiceMade = false
  useLatestVersion = await selectLatestTemplateVersion(version)

  expect(useLatestVersion).toBe(false)

  stdoutWriteMock.mockRestore()
})

test('Selects template version', async () =>
{
  const stdoutWriteMock = jest.spyOn(process.stdout, 'write').mockImplementation((value) =>
  {
    if (value.includes(Message.ChooseVersionTagged) && !choiceMade)
    {
      choiceMade = true
      choicesMade++

      process.stdin.emit('keypress', choicesMade === 1 ? EOL : `${DOWN}${EOL}`)
    }
  })
  const versions = ['0.0.1', '0.0.2']
  let choiceMade = false
  let choicesMade = 0
  let version = await selectOtherTemplateVersion(versions)

  expect(version).toBe('0.0.1')

  choiceMade = false
  version = await selectOtherTemplateVersion(versions)

  expect(version).toBe('0.0.2')

  stdoutWriteMock.mockRestore()
})

/*
test('Selects first template', async () =>
{
  let templateChosen = false
  let template

  const stdoutWriteMock = jest.spyOn(process.stdout, 'write').mockImplementation((value) =>
  {
    if (value.includes('template') && !templateChosen)
    {
      templateChosen = true

      process.stdin.emit('keypress', EOL)
    }
  })

  template = await selectTemplate()

  expect(template.name).toBe('template-1')
  expect(template.repository).toEqual({fork:'ryor-org', name:'ryor-template-1'})
  expect(template.version).toBe('master')

  stdoutWriteMock.mockRestore()
})

test('Selects second template and master version', async () =>
{
  let templateChosen = false
  let versionChosen = false
  let template

  const stdoutWriteMock = jest.spyOn(process.stdout, 'write').mockImplementation((value) =>
  {
    if (value.includes('template') && !templateChosen)
    {
      templateChosen = true

      process.stdin.emit('keypress', `${DOWN}${EOL}`)
    }

    else if (value.includes('version') && !versionChosen)
    {
      versionChosen = true

      process.stdin.emit('keypress', EOL)
    }
  })

  template = await selectTemplate()

  expect(template.name).toBe('template-2')
  expect(template.repository).toEqual({fork:'ryor-org', name:'ryor-template-2'})
  expect(template.version).toBe('master')

  stdoutWriteMock.mockRestore()
})

test('Selects second template and first tagged version', async () =>
{
  let templateChosen = false
  let versionChosen = false
  let template

  const stdoutWriteMock = jest.spyOn(process.stdout, 'write').mockImplementation((value) =>
  {
    if (value.includes('template') && !templateChosen)
    {
      templateChosen = true

      process.stdin.emit('keypress', `${DOWN}${EOL}`)
    }

    else if (value.includes('version') && !versionChosen)
    {
      versionChosen = true

      process.stdin.emit('keypress', `${DOWN}${EOL}`)
    }
  })

  template = await selectTemplate()

  expect(template.name).toBe('template-2')
  expect(template.repository).toEqual({fork:'ryor-org', name:'ryor-template-2'})
  expect(template.version).toBe('1.0.0')

  stdoutWriteMock.mockRestore()
})
*/
