import {existsSync, moveSync, writeFileSync} from 'fs-extra'
import {EOL} from 'os'
import path from 'path'
import List from 'prompt-list'
import {exec, rm, which} from 'shelljs'
import * as log from './log'
import {selectPaths} from './paths'
import {fetchTemplate, selectTemplate} from './template'

export const README:string = `## [PROJECT_NAME]${EOL}---${EOL}###### Made with **[ryor](https://github.com/ryor-org/ryor)**.`
export const TEMPLATE_ONLY_ASSETS:string[] = ['.circleci', 'run/tools/circleci.js']
export const TEMPLATE_ONLY_DEPENDENCIES:string[] = ['codecov']

export const Choice:{[key:string]:string} = {
  Confirm: 'Cool.',
  Decline: `Eh, I'm having second thoughts...`
}

export const Message:{[key:string]:string} = {
  ConfirmConfiguration: 'Project will be created at [PROJECT_PATH] using version [TEMPLATE_VERSION] the [TEMPLATE_NAME] template. Cool?',
  ConfiguringProject: 'Configuring project...',
  InstallingDependencies: 'Installing dependencies...',
  Success: 'Project created at [PROJECT_PATH]'
}

export async function configureProject():Promise<Project>
{
  const paths:Paths = await selectPaths()
  const template:Template = await selectTemplate()

  return {paths, template}
}

export async function confirmProject({paths, template:{repository, version}}:Project):Promise<boolean>
{
  const confirmed:string = await new List({
    message: Message.ConfirmConfiguration
      .replace('[PROJECT_PATH]', paths.project)
      .replace('[TEMPLATE_VERSION]', version.replace('v', ''))
      .replace('[TEMPLATE_NAME]', repository.split('ryor-')[1]),
    name: 'confirmed',
    choices: [Choice.Confirm, Choice.Decline]
  }).run()

  return confirmed === Choice.Confirm
}

export async function createProject({paths, template}:Project):Promise<void>
{
  paths.template = await fetchTemplate(template)

  configureTemplate({paths, template})

  if (paths.backup !== undefined)
    moveSync(paths.project, paths.backup)

  moveSync(paths.template, paths.project)

  await installDependencies(paths.project)

  log.okay(Message.Success.replace('[PROJECT_PATH]', paths.project))
}

export function configureTemplate({paths, template}:Project):void
{
  const templateName:string = template.repository.split('/')[1]
  const templatePath:string = paths.template!
  const projectName:string = path.parse(paths.project).name

  log.wait(Message.ConfiguringProject)

  deleteTemplateOnlyFiles(templatePath)
  configurePackage(templatePath, templateName, projectName)
  writeReadme(templatePath, projectName)

  log.done()
}

export function deleteTemplateOnlyFiles(templatePath:string):void
{
  TEMPLATE_ONLY_ASSETS.forEach((file:string):void =>
  {
    const filePath:string = path.resolve(templatePath, file)

    if (existsSync(filePath))
      rm('-rf', filePath)
  })
}

export function configurePackage(templatePath:string, templateName:string, projectName:string):void
{
  const packageJSONPath:string = path.resolve(templatePath, 'package.json')
  const packageJSON:Package = require(packageJSONPath) as Package
  const spacing:number = 2

  packageJSON.version = '0.0.0'

  delete packageJSON.description

  if (packageJSON.dependencies !== undefined)
    TEMPLATE_ONLY_DEPENDENCIES.forEach((dependency:string):boolean => delete packageJSON.dependencies![dependency])

  if (packageJSON.devDependencies !== undefined)
    TEMPLATE_ONLY_DEPENDENCIES.forEach((dependency:string):boolean => delete packageJSON.devDependencies![dependency])

  writeFileSync(
    packageJSONPath,
    JSON.stringify(packageJSON, undefined, spacing).replace(new RegExp(templateName, 'g'), projectName.split(' ').join('-').toLowerCase())
  )
}

export function writeReadme(templatePath:string, projectName:string):void
{
  writeFileSync(path.resolve(templatePath, 'README.md'), README.replace('[PROJECT_NAME]', projectName))
}

export function installDependencies(projectPath:string):Promise<void>
{
  return new Promise<void>((resolve:() => void):void =>
  {
    const cwd:string = process.cwd()

    log.wait(Message.InstallingDependencies)

    process.chdir(projectPath)

    exec(which('yarn') !== null ? 'yarn' : 'npm install', {silent:true}, ():void =>
    {
      process.chdir(cwd)

      log.done()

      resolve()
    })
  })
}
