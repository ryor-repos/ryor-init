declare module 'download' {
  function download(url:string, path?:string, config?:{}):Promise<Buffer>
  export default download
}

declare module 'prompt-base' {
  interface Options {
    message: string
    name: string
  }
  class Prompt {
    public constructor(options:Options)
    public run():Promise<string|undefined>
  }
  export default Prompt
}

declare module 'prompt-list' {
  interface Options {
    message: string
    name: string
    choices: string[]
  }
  class List {
    public constructor(options:Options)
    public run():Promise<string>
  }
  export default List
}

declare module 'shelljs' {
  export function exec(command:string, options:{}, callback:() => void):void
  export function rm(options:string, path:string):void
  export function which(command:string):string|null
}

interface GithubTag {
  name: string
}

interface Package {
  name: string
  version: string
  description?: string
  dependencies?: {[key:string]:string}
  devDependencies?: {[key:string]:string}
}

interface Paths {
  project: string
  backup?: string
  template?: string
}

interface Project {
  paths: Paths
  template: Template
}

interface Template {
  repository: string
  version: string
}

interface TemplatesListCategory {
  type: string
  templates: string[]
}
