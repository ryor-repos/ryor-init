import {bold, cyan} from 'chalk'

export function wait(message:string):void
{
  process.stdout.write(`${cyan('⏳')} ${bold(message)}`)
}

export function done(message:string = 'done'):void
{
  console.log(` ${cyan(message)}`)
}

export function okay(message:string):void
{
  console.log(`${cyan('☺')} ${bold(message)}`)
}
