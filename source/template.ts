import download from 'download'
import {tmpdir} from 'os'
import {resolve} from 'path'
import List from 'prompt-list'
import * as log from './log'

export const Choice:{[key:string]:string} = {
  Confirm: 'Yes',
  Decline: 'No'
}

export const Message:{[key:string]:string} = {
  ChooseRepository: 'Which template would you like to use?',
  ChooseType: 'Which type of template would you like to use?',
  ChooseVersionLatest: 'Use latest version (recommended)?',
  ChooseVersionTagged: 'Which version would you like to use?',
  DownloadingArchive: 'Downloading archive...'
}

export const URL:{[key:string]:string} = {
  GithubArchive: 'https://github.com/[REPOSITORY]/archive/[VERSION].tar.gz',
  GithubTagsAPI: 'https://api.github.com/repos/[REPOSITORY]/tags',
  TemplatesList: 'https://raw.githubusercontent.com/ryor-org/ryor-templates-list/master/ryor-templates-list.json'
}

export async function selectTemplate():Promise<Template>
{
  const category:TemplatesListCategory = await selectTemplatesListCategory()
  const repository:string = await selectTemplateRepository(category)
  const version:string = await selectTemplateVersion(repository)

  return {repository, version}
}

export async function selectTemplatesListCategory():Promise<TemplatesListCategory>
{
  const templatesList:TemplatesListCategory[] = await fetchTemplatesList()
  const types:string[] = templatesList.map((category:TemplatesListCategory):string => category.type)
  const type:string = await new List({
    message: Message.ChooseType,
    name: 'type',
    choices: types
  }).run()

  return templatesList.find((category:TemplatesListCategory):boolean => category.type === type)!
}

export async function selectTemplateRepository({templates}:TemplatesListCategory):Promise<string>
{
  const prefix:string = 'ryor/ryor-'
  const template:string = await new List({
    message: Message.ChooseRepository,
    name: 'repository',
    choices: templates.map((id:string):string => id.replace(prefix, ''))
  }).run()

  return `${prefix}${template}`
}

export async function selectTemplateVersion(repository:string):Promise<string>
{
  const tags:string[] = await fetchRepositoryTags(repository)

  if (tags.length === 0)
    return 'master'

  const latestVersion:string = tags[0]
  const useLatestVersion:boolean = await selectLatestTemplateVersion(latestVersion)

  if (useLatestVersion)
    return latestVersion

  return await selectOtherTemplateVersion(tags)
}

export async function selectLatestTemplateVersion(version:string):Promise<boolean>
{
  const choice:string = await new List({
    message: Message.ChooseVersionLatest,
    name: 'choice',
    choices: [Choice.Confirm, Choice.Decline]
  }).run()

  return choice === Choice.Confirm
}

export async function selectOtherTemplateVersion(tags:string[]):Promise<string>
{
  return await new List({
    message: Message.ChooseVersionTagged,
    name: 'version',
    choices: tags
  }).run()
}

export async function fetchTemplatesList():Promise<TemplatesListCategory[]>
{
  const buffer:Buffer = await download(URL.TemplatesList)

  return JSON.parse(buffer.toString()) as TemplatesListCategory[]
}

export async function fetchRepositoryTags(repository:string):Promise<string[]>
{
  const buffer:Buffer = await download(URL.GithubTagsAPI.replace('[REPOSITORY]', repository))

  return (JSON.parse(buffer.toString()) as GithubTag[]).map(({name}:GithubTag):string => name)
}

export async function fetchTemplate({version, repository}:Template):Promise<string>
{
  const tempDirectoryPath:string = resolve(tmpdir(), 'ryor')

  log.wait(Message.DownloadingArchive)

  await download(URL.GithubArchive.replace('[REPOSITORY]', repository).replace('[VERSION]', version), tempDirectoryPath, {extract:true})

  log.done()

  return resolve(tempDirectoryPath, `${repository.split('/')[1]}-${version.replace('v', '')}`)
}
