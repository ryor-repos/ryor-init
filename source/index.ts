import * as log from './log'
import {configureProject, confirmProject, createProject} from './project'

const Message:{[key:string]:string} =
{
  Abort: 'No worries, bye!'
}

export async function run():Promise<void>
{
  console.log('')

  try
  {
    const project:Project = await configureProject()
    const confirmed:boolean = await confirmProject(project)

    if (confirmed)
      await createProject(project)

    else
      log.okay(Message.Abort)
  }

  catch (error)
  {
    console.log('')
    console.error(error)
  }

  console.log('')
}
